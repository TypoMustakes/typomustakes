# Hi!

This is my custom repository, consisting of packages that either I made or that serve as archives (p.e. stuff deleted from the AUR).

# How to add

> ## You will need...
> 
> > - root privileges
> >
> > - pacman
> 
> ## and then
> 
> > - open the `/etc/pacman.conf` file with your preferred text editor (with root privileges)
> >
> > - add the following lines to the end of the file:
> >
> > > `[typomustakes]`
> > >
> > > `SigLevel = Optional TrustAll`
> > >
> > > `Server = https://gitlab.com/TypoMustakes/typomustakes/-/raw/master`
> >
> > - update your database:
> >
> > > `sudo pacman -Sy`

Enjoy! 😄



I know this project isn't much, but if you still wish to support me, even by just buying me a coffee, please consider supporting me via [PayPal](https://paypal.me/pools/c/8A6cgaZnse). Thanks 💓
